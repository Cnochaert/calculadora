# Calculadora 
import os

# Funcion Menu
def menu() :
    os.system("cls")
    print("Seleccione la Operación (1,2,3,4,9)")
    print("1. Suma")
    print("2. Resta")
    print("3. Multiplicación")
    print("4. División")
    print("9. Salir")
    #Casteo a int el dato tomado desde el input
    opcion = int(input("\n Ingrese una Opción "))

    if (opcion != 1 and opcion != 2 and opcion != 3 and opcion != 4 and opcion != 9):  
        opcion = 0

    return opcion

# Muestro el Resultado
def resultado(seleccion):
    operador1 = int(input("Ingrese el primer número ")) 
    operador2 = int(input("Ingrese el segundo número ")) 
    if (seleccion==1):
         resultado = operador1+operador2
    elif (seleccion == 2):
         resultado = operador1-operador2
    elif (seleccion == 3):
         resultado = operador1*operador2
    elif (seleccion == 4):
          # Controlo división por cero
          try:
               resultado = operador1 / operador2
          except ZeroDivisionError :
               resultado = 0
          finally :
               return resultado
    return resultado


# Programa Principal      
continua = True 
while (continua) :
    seleccion = menu()
    
    if (seleccion != 0 and seleccion != 9):
        respuesta = resultado(seleccion)
        print(str(respuesta))
        print("Continua con otra operación (s/n) ?")
        if (input()=="s" or input()=="S"):
            continua = True
        else:
             continua = False
    elif (seleccion==0):
        print("Opción Incorrecta")
        continua = False

    elif (seleccion==9):
        continua = False
   
        
print("Fin del Programa")






